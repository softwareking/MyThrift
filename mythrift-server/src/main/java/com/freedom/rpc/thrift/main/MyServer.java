package com.freedom.rpc.thrift.main;

import java.net.InetSocketAddress;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;

import com.freedom.rpc.thrift.common.service.HelloWorldService;
import com.freedom.rpc.thrift.common.service.impl.HelloWorldServiceImpl;
import com.freedom.rpc.thrift.common.utils.LoggerUtils;
import com.freedom.rpc.thrift.registry.MyPublishRunnable;
import com.freedom.rpc.thrift.server.utils.MyServerProperties;

//相关Thrift文档可参考：http://my.oschina.net/qiangzigege/blog/495920
public class MyServer {
	private static final Logger logger = LogManager.getLogger(MyServer.class);
	static {
		// 加载连接池
		try {
			Class.forName("com.freedom.rpc.thrift.registry.JedisPoolManager");
		} catch (ClassNotFoundException e) {
			LoggerUtils.error(logger, e.toString());
			System.exit(-1);
		}
	}

	//
	public static void main(String[] args) {
		//
		LoggerUtils.info(logger, "---------------------------------------------------------------");
		MyServerProperties property = MyServerProperties.getInstance();
		LoggerUtils.info(logger, property.toString());
		try {
			InetSocketAddress address = new InetSocketAddress(property.getIp(), property.getPort());
			TNonblockingServerTransport serverTransport = new TNonblockingServerSocket(address);
			TFramedTransport.Factory transportFactory = new TFramedTransport.Factory();
			// 设置协议工厂为 TBinaryProtocol.Factory
			TBinaryProtocol.Factory protocolFactory = new TBinaryProtocol.Factory();
			// 关联处理器与 Hello 服务的实现
			// TProcessor processor = new
			// HelloWorldService.Processor<HelloWorldService.Iface>(
			// new HelloWorldServiceImpl());
			TMultiplexedProcessor processor = new TMultiplexedProcessor();
			processor.registerProcessor("HelloWorldService",
					new HelloWorldService.Processor<HelloWorldService.Iface>(new HelloWorldServiceImpl()));

			TThreadedSelectorServer.Args tArgs = new TThreadedSelectorServer.Args(serverTransport);
			tArgs.transportFactory(transportFactory);
			tArgs.protocolFactory(protocolFactory);
			tArgs.processor(processor);
			{// 以下属于服务器端的参数优化
				// accept线程默认就是1，所以不设置了
				tArgs.selectorThreads(property.getSelectorThreads());
				tArgs.acceptQueueSizePerThread(property.getQueueSize());
				tArgs.workerThreads(property.getWorkerThreads());
				LoggerUtils.info(logger, "***************");
				LoggerUtils.info(logger, "setting of thrift server is as follows:");
				LoggerUtils.info(logger, "selector threads: " + tArgs.getSelectorThreads());
				LoggerUtils.info(logger, "queue size: " + tArgs.getAcceptQueueSizePerThread());
				LoggerUtils.info(logger, "worker threads: " + tArgs.getWorkerThreads());
				LoggerUtils.info(logger, "***************");
			}
			TServer server = new TThreadedSelectorServer(tArgs);
			LoggerUtils.info(logger, "Start thrift server on port " + property.getPort() + "...");
			LoggerUtils.info(logger, "---------------------------------------------------------------");
			// 在这里启动心跳线程
			String[] servers = property.getRedisServers();
			for (String redisServer : servers) {// 每个server启动一个线程
				redisServer = redisServer.trim();
				new Thread(new MyPublishRunnable(redisServer)).start();
			}
			LoggerUtils.info(logger, "Heart beat thread start...");
			//
			// 继续server
			server.serve();
			//
		} catch (TTransportException e) {
			LoggerUtils.error(logger, "exception as :" + e.toString());
		} finally {
			LoggerUtils.info(logger, "thrift server exit... @" + System.currentTimeMillis());
			System.exit(0);
		}
	}

}
