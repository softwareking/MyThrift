### **MyThrift使用文档**:bowtie:

版本说明

刘志强         0.0.1        创建       2016-05-04 06:26:00

--------------------------------------------------------------------------------------------------------

**文档目录：**

1)Thrift编译环境搭建

2)编写接口文件&生成对应的java文件

3)编写业务逻辑java文件&融入java框架

4)打包&部署到linux里执行







**文档内容：**

1)Thrift编译环境搭建

首先，用户需要知道怎么搭建thrift的编译环境

http://thrift.apache.org/docs/install/   可以看到有好几个选项

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/215231_cfde08d6_70679.png "在这里输入图片标题")

结合自己的机器环境，选择相应的超链接，进去按照指示来搭建一个thrift的编译环境

{
//不过这里有个需要注意的地方，

比如说你选择的是centos  http://thrift.apache.org/docs/install/centos

然后在最下方的小节[之前的环境准备还是照做不误]：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0505/121453_faf80e1f_70679.png "在这里输入图片标题")
    
这个地方如果按照图片的步骤，实际安装的是1.0.0版本的，有点问题

所以强烈建议换种方式，如下所示： 打开 http://thrift.apache.org/download

![输入图片说明](http://git.oschina.net/uploads/images/2016/0505/121720_2bf4d4b5_70679.png "在这里输入图片标题")


 红色方框内的网址实际上是： http://www.apache.org/dyn/closer.cgi?path=/thrift/0.9.3/thrift-0.9.3.tar.gz

下载到你的linux机器上，解压缩，然后依次执行下列命令：

./configure --with-lua=no

make

sudo make install


}

看到下面的图片表示安装成功

![输入图片说明](http://git.oschina.net/uploads/images/2016/0505/122055_ecce51f1_70679.png "在这里输入图片标题")


2)编写接口文件&生成对应的java文件

必须知道的是，thrift需要先定义一个 xx.thrift文件，此文件用来定义类似于interface的若干接口

下面举个例子，我有一个HelloWorldService.thrift文件，内容如下：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/220546_e3f10dbc_70679.png "在这里输入图片标题")

那么，用户会问“我怎么知道有什么类型呢”，别急，见文档 http://thrift.apache.org/docs/types

PS:第一个参数前面就是1，第2个参数就是2，比如

string helloWorldString(1:string content, 2:string param2 , 3:string param3)

好，现在已经有了接口文件，然后如何生成对应的java文件呢？

因为是为了生成java文件，所以执行

thrift --gen java HelloWorldService.thrift

然后就会看到当前文件夹有个gen-java文件夹，如图所示：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/221402_516970b1_70679.png "在这里输入图片标题")

可以看到这个文件很大，这是因为thrift已经帮我们做好了很多额外的工作，比如序列化和反序列化，这样我们才可以很轻松的享受便利！


3)业务java文件融入java框架

现在已经有了这个文件，如何融入到我们的框架当中呢？ 而且我们也没有写自己的业务逻辑，别急，下面就来处理这个问题。

先将master的代码导入到eclipse里，然后你看到的是这样的

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/223237_52cc0f1d_70679.png "在这里输入图片标题")

至于项目的结构，我就不用细说了，各位自己研究下pom.xml即可。


好，现在已经导入了工程，之前的xx.thrift文件和生成的xx.java文件怎么处理？

见下图：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/223745_c159ae8c_70679.png "在这里输入图片标题")

强烈建议按照此结构来放置文件，当然，如果you know what you are doing,那也没问题 :)

然后注意到上图还有一个文件，HelloWorldServiceImpl.java，其实就是实现类，上图：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/225348_b0990b1f_70679.png "在这里输入图片标题")

那么，到这里，我们实际上，已经完成了
1）定义xx.thrift
2) 根据xx.thrift生成接口类
3) server端的业务实现类

那么新的问题来了，如何在client端和server端使用呢？ 下面就来解决这个问题

先看client端如何使用，见mythrift-client工程里的类

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/230032_8fe30b41_70679.png "在这里输入图片标题")

再来看看服务端的代码，见mythrift-server工程里的MyServer.java类

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/231544_78f9fb30_70679.png "在这里输入图片标题")

这个时候在eclipse里启动server工程里的MyServer.java,再启动client工程里的MyClient.java
就可以看到输出了

恭喜，你在eclipse里完成了工程代码的编写和运行。

4)打包&部署到linux里执行

显然，我们不会只在eclipse里运行，下面就讲解如何部署到linux里，以及如何配置相应的参数

4.1)编译整个工程

首先需要在windows里把eclipse里的server|client工程重新编译成jar包(这是因为你添加了自己的业务代码，所以要重新打包)

在工程的根路径下执行 mvn -X clean compile package 这样就可以同时编译client工程和server工程


编译成功后的图片是这样的

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/233927_9651eb56_70679.png "在这里输入图片标题")


4.2)部署server端二进制文件

将二进制文件thrift-server-binary-[版本号].tar.gz解压缩在linux下某个目录下
然后将4.1中编译好的mythrift-server/target/mythrift-server-[版本号]-jar-with-dependencies.jar
替换之前解压缩的目录/lib/对应的jar包，注意，是替换，老的jar包需要删除。

然后审查linux环境下的conf目录里的各个配置文件-log4j.properties ,server.properties的配置是否正确

然后启动./run.sh即可。

4.3)部署client端二进制文件

将二进制文件thrift-client-binary-[版本号].tar.gz解压缩在linux下某个目录下
然后将4.1中编译好的mythrift-client/target/mythrift-client-[版本号]-jar-with-dependencies.jar
替换之前解压缩的目录/lib/对应的jar包，注意，是替换，老的jar包需要删除。

然后审查linux环境下的conf目录里的各个配置文件-log4j.properties ,client.properties的配置是否正确

然后启动./run.sh即可,然后查看日志文件(logs目录下)的输出，确认是否成功执行！



这样，一个简单的RPC服务就编写完毕，

本版本0.0.1 在client和server间部署负载均衡组件，然后client的IP和端口指向此组件，就成了一个集中式负载均衡的案例。

完全达到了生产级别，可放心使用。



下一个版本引入zookeeper做服务发现和治理。




其它友情链接：

http://archive.apache.org/dist/thrift/

http://www.open-open.com/lib/view/open1452062361511.html
