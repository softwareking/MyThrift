package com.freedom.rpc.thrift.client.socket;

import org.apache.thrift.transport.TSocket;

/**
 * 自己封装的socket
 * 
 * @author gqliu 2016年5月4日
 *
 */
public class MyTSocket extends TSocket {

	private boolean alive = true;

	public MyTSocket(String host, int port) {
		super(host, port);
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

}
