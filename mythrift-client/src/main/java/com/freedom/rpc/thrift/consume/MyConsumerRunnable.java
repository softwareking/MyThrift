package com.freedom.rpc.thrift.consume;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.freedom.rpc.thrift.client.queue.MyHeartbeatQueue;
import com.freedom.rpc.thrift.common.utils.LoggerUtils;
import com.freedom.rpc.thrift.common.utils.TimeUtils;

public class MyConsumerRunnable implements Runnable {
	private static final Logger logger = LogManager.getLogger(MyConsumerRunnable.class);
	private String channel;

	public MyConsumerRunnable(String c) {
		channel = c.trim();
	}

	@Override
	public void run() {

		while (true) {
			// 尝试取地址
			String address = MyHeartbeatQueue.getObject(channel);
			// 取地址失败
			if (null == address) {
				// 趁这个时间，可以
				LoggerUtils.debug(logger, "no message fetched");
				continue;
			}
			// 取地址成功
			LoggerUtils.debug(logger, address + " ->channel: " + channel);

		}

	}

}
