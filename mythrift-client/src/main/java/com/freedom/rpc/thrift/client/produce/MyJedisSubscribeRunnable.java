package com.freedom.rpc.thrift.client.produce;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.freedom.rpc.thrift.client.utils.MyJedisProperties;
import com.freedom.rpc.thrift.common.utils.LoggerUtils;

import redis.clients.jedis.Jedis;

public class MyJedisSubscribeRunnable implements Runnable {
	private static final Logger logger = LogManager.getLogger(MyJedisSubscribeRunnable.class);
	private String ip;
	private int port;

	public MyJedisSubscribeRunnable(String i, int p) {
		ip = i;
		port = p;
	}

	@Override
	public void run() {
		LoggerUtils.info(logger, "subscribe thread [" + ip + ":" + port + "] is running..." + this.toString());
		while (true) {
			Jedis jedisObject = null;
			try {
				// 初始化连接
				jedisObject = new Jedis(ip, port);
				// 一次订阅多个频道
				jedisObject.subscribe(MyMessageListener.getInstance(),
						MyJedisProperties.getInstance().getServiceChannels());
				// 阻塞
			} catch (Exception e) {
				LoggerUtils.error(logger, e.toString());
			} finally {
				if (null != jedisObject) {
					try {
						jedisObject.close();
					} catch (Exception e) {
						LoggerUtils.error(logger, e.toString());
					}
				}
			}
		}

	}
}
