package com.freedom.rpc.thrift.client.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.freedom.rpc.thrift.common.utils.LoggerUtils;

import org.apache.log4j.LogManager;

/**
 * 
 * @author zhiqiang.liu
 * @2016年5月30日
 *
 */

public class MyJedisProperties {
	// 以下为全局需要
	private static final Logger logger = LogManager.getLogger(MyJedisProperties.class);

	// 测试
	public static void main(String[] args) {
		// just for test
		MyJedisProperties property = MyJedisProperties.getInstance();
		LoggerUtils.debug(logger, property.toString());
	}

	public static MyJedisProperties getInstance() {
		return myProperties;
	}

	//
	private static MyJedisProperties myProperties = null;// 全局单例变量，一开始就存在
	static {// 静态块里，只加载一次

		Properties props = new Properties();
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(JEDIS_CONFIG.JEDIS_CONFIG_FILE));
			// Thread.currentThread().getContextClassLoader().getResourceAsStream(MyConstants.CONFIG_FILE);
			props.load(in);
			in.close();
		} catch (Exception e) {
			// logger.error(e.toString());
			LoggerUtils.error(logger, "fail to read config file " + JEDIS_CONFIG.JEDIS_CONFIG_FILE);
			System.exit(-1);
		}
		// 读取值
		LoggerUtils.debug(logger, "succeed to read config file " + JEDIS_CONFIG.JEDIS_CONFIG_FILE);
		//
		// redis config
		String redisConfig = props.getProperty(JEDIS_CONFIG.REDIS_SERVERS, "");
		if (null == redisConfig || redisConfig.trim().length() == 0) {// 无效redis设置，退出
			LoggerUtils.error(logger, "no redis servers defined in config file " + JEDIS_CONFIG.JEDIS_CONFIG_FILE);
			System.exit(-1);
		}
		redisConfig = redisConfig.trim();
		// redis servers
		String[] redisServers = redisConfig.split(",");
		if (null == redisServers || redisServers.length == 0) {
			LoggerUtils.error(logger,
					"no valid redis servers defined in config file " + JEDIS_CONFIG.JEDIS_CONFIG_FILE);
			System.exit(-1);
		}
		// 判断每个server的有效
		for (String server : redisServers) {
			server = server.trim();
			String[] ipAndPort = server.split(":");
			if (null == ipAndPort || ipAndPort.length != 2) {
				LoggerUtils.error(logger, "no valid redis servers [ " + server + " ] defined in config file "
						+ JEDIS_CONFIG.JEDIS_CONFIG_FILE);
				System.exit(-1);
			}
			//
			try {// 必须为合法数字，否则报错
				Integer.parseInt(ipAndPort[1]);
			} catch (Exception e) {
				LoggerUtils.error(logger, "no valid redis servers [ " + server + " ] defined in config file "
						+ JEDIS_CONFIG.JEDIS_CONFIG_FILE);
				System.exit(-1);
			}

		}
		//
		// 订阅的频道
		String serviceChannels = props.getProperty(JEDIS_CONFIG.SEERVICE_CHANNELS, "");
		if (null == serviceChannels || serviceChannels.trim().length() == 0) {
			LoggerUtils.error(logger,
					"no valid service channels  defined in config file " + JEDIS_CONFIG.JEDIS_CONFIG_FILE);
			System.exit(-1);
		}
		// 结束
		props.clear();
		props = null;
		// 构造新的对象
		myProperties = new MyJedisProperties(redisServers, serviceChannels.split(","));
		LoggerUtils.info(logger, "succeed to create my jedis properties object ");
		LoggerUtils.info(logger, "cpu - " + Runtime.getRuntime().availableProcessors());
	}

	// 私有属性开始//////////////////////////////////////////////////////////////////
	// thrift

	private String[] redisServers;
	private String[] serviceChannels;

	private MyJedisProperties() {// 私有方法，保证单例
	}

	private MyJedisProperties(String[] rServers, String[] sChannels) {

		this.redisServers = rServers;
		this.serviceChannels = sChannels;
	}

	public String[] getRedisServers() {
		return this.redisServers;
	}

	public String[] getServiceChannels() {
		return serviceChannels;
	}

	public String toString() {
		StringBuilder strBuilder = new StringBuilder("");
		return strBuilder.toString();
	}

	// 内部类
	static class JEDIS_CONFIG {
		public static String JEDIS_CONFIG_FILE = System.getProperty("jedisProperties",
				"src/main/resources/jedis.properties");
		// redis_servers
		public static String REDIS_SERVERS = "redis_servers";
		public static String SEERVICE_CHANNELS = "service_channels";
	}
}
