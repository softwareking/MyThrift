package com.freedom.rpc.thrift.client.produce;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.freedom.rpc.thrift.client.queue.MyHeartbeatQueue;
import com.freedom.rpc.thrift.common.utils.LoggerUtils;

import redis.clients.jedis.JedisPubSub;

public class MyMessageListener extends JedisPubSub {
	private MyMessageListener() {

	}

	public void onMessage(String channel, String message) {
		//LoggerUtils.info(logger, "receive from channel[" + channel + "] msg[" + message + "]");
		// 注意：这里放入的，都是成功的保活信息,失败的，就不会取到了
		MyHeartbeatQueue.addObject(channel.trim(), message.trim());
	}

	public void onSubscribe(String channel, int subscribedChannels) {
		LoggerUtils.info(logger, "onSubscribe invoked---" + channel);
	}

	public void onUnsubscribe(String channel, int subscribedChannels) {
		LoggerUtils.info(logger, "onUnsubscribe invoked");
	}

	private static final Logger logger = LogManager.getLogger(MyMessageListener.class);
	private static MyMessageListener instance = null;

	public static MyMessageListener getInstance() {
		return instance;
	}

	static {
		instance = new MyMessageListener();
	}
}
