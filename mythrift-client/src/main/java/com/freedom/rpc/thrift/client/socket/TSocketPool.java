package com.freedom.rpc.thrift.client.socket;

import java.util.Properties;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.freedom.rpc.thrift.client.utils.MyClientProperties;

/**
 * 连接池
 * 
 * @author gqliu 2016年5月4日
 *
 */
public class TSocketPool {

	private GenericObjectPool<MyTSocket> socketPool;

	// public TSocketPool(InputStream in) {
	// Properties pro = new Properties();
	// try {
	// pro.load(in);
	// } catch (Exception e) {
	// e.printStackTrace();
	// System.exit(-1);
	// }
	// // 初始化对象池配置
	// GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
	// poolConfig.setBlockWhenExhausted(Boolean.parseBoolean(pro.getProperty("thrift_blockWhenExhausted")));
	// poolConfig.setMaxWaitMillis(Long.parseLong(pro.getProperty("thrift_maxWait")));
	// poolConfig.setMinIdle(Integer.parseInt(pro.getProperty("thrift_minIdle")));
	// poolConfig.setMaxIdle(Integer.parseInt(pro.getProperty("thrift_maxIdle")));
	// poolConfig.setMaxTotal(Integer.parseInt(pro.getProperty("thrift_maxTotal")));
	// poolConfig.setTestOnBorrow(Boolean.parseBoolean(pro.getProperty("thrift_testOnBorrow")));
	// poolConfig.setTestOnReturn(Boolean.parseBoolean(pro.getProperty("thrift_testOnReturn")));
	// poolConfig.setTestOnCreate(Boolean.parseBoolean(pro.getProperty("thrift_testOnCreate")));
	// poolConfig.setTestWhileIdle(Boolean.parseBoolean(pro.getProperty("thrift_testWhileIdle")));
	// poolConfig.setLifo(Boolean.parseBoolean(pro.getProperty("thrift_lifo")));
	// // 初始化对象池
	// socketPool = new GenericObjectPool<MyTSocket>(
	// new TSocketFactory(pro.getProperty("thrift_ip"),
	// Integer.parseInt(pro.getProperty("thrift_port"))), poolConfig);
	// }
	private TSocketPool() {
	}

	private TSocketPool(Properties pro) {
		// 初始化对象池配置
		GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
		poolConfig.setBlockWhenExhausted(Boolean.parseBoolean(pro.getProperty("thrift_blockWhenExhausted")));
		poolConfig.setMaxWaitMillis(Long.parseLong(pro.getProperty("thrift_maxWait")));
		poolConfig.setMinIdle(Integer.parseInt(pro.getProperty("thrift_minIdle")));
		poolConfig.setMaxIdle(Integer.parseInt(pro.getProperty("thrift_maxIdle")));
		poolConfig.setMaxTotal(Integer.parseInt(pro.getProperty("thrift_maxTotal")));
		poolConfig.setTestOnBorrow(Boolean.parseBoolean(pro.getProperty("thrift_testOnBorrow")));
		poolConfig.setTestOnReturn(Boolean.parseBoolean(pro.getProperty("thrift_testOnReturn")));
		poolConfig.setTestOnCreate(Boolean.parseBoolean(pro.getProperty("thrift_testOnCreate")));
		poolConfig.setTestWhileIdle(Boolean.parseBoolean(pro.getProperty("thrift_testWhileIdle")));
		poolConfig.setLifo(Boolean.parseBoolean(pro.getProperty("thrift_lifo")));
		// 初始化对象池
		socketPool = new GenericObjectPool<MyTSocket>(
				new TSocketFactory(pro.getProperty("thrift_ip"), Integer.parseInt(pro.getProperty("thrift_port"))),
				poolConfig);
	}

	public MyTSocket borrowObject() throws Exception {
		return socketPool.borrowObject();
	}

	public void returnObject(MyTSocket socket) {
		socketPool.returnObject(socket);
	}

	//
	//
	//
	// 单例模式
	private static TSocketPool instance = null;
	static {
		instance = new TSocketPool(MyClientProperties.getInstance());
	}

	// 获取此单例
	public static TSocketPool getInstance() {
		return instance;
	}
}
