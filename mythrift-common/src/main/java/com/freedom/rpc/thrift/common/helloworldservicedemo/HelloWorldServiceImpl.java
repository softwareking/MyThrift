package com.freedom.rpc.thrift.common.helloworldservicedemo;

import org.apache.thrift.TException;

public class HelloWorldServiceImpl implements HelloWorldService.Iface {

	public String helloWorldString(String content) throws TException {
		return "yes,return " + content;
	}

	public boolean helloWorldBoolean(int number) throws TException {
		return false;
	}

}
